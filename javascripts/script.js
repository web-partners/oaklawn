(function($){
	$(document).ready(function(){
		var nextSection = $('a.next-section');
		$('html').removeClass('no-js');
		nextSection.on('click', function(e){
			var loc = $(this).attr('href');
			e.preventDefault();
			$('html, body').animate({scrollTop: $(loc).offset().top}, 1000);
		});
		$('.desktop-nav').find('li ul').hide();
		$('.desktop-nav').find('.nav>ul>li>ul').each(function(){
			if ($(this).outerWidth() > $(this).closest('li').outerWidth())
			{
				$(this).css('margin-left', -(($(this).outerWidth() - $(this).closest('li').outerWidth())/2));
			}
		})
		$('.desktop-nav').find('.nav>ul>li').on('mouseover', function(){
			$(this).find('ul').stop().slideDown(200);
		}).on('mouseout', function(){
			$(this).find('ul').stop().slideUp(200);
		});
		$('.dropdown').on('show.bs.dropdown', function (){
			$(this).siblings('.open').removeClass('open').find('a.dropdown-toggle').attr('data-toggle', 'dropdown');
			$(this).find('a.dropdown-toggle').removeAttr('data-toggle');
		});
		$('.form').find('.form-control').on('focus', function(){
			if(this.value == this.defaultValue) {
				this.value = ''; 
			}
		}).on('blur', function(){
			if(this.value == '') {
				this.value = this.defaultValue;
			}
		});
	});
})(jQuery);